<?php
    include 'conexion.php';
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/estilos.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <title>Mesas</title>
</head>
<body>
    <?php
        if(isset($_POST['Mesa'])){
            $estado=$_POST['estado'];
            $numero_de_la_mesa=$_POST['numero_de_la_mesa'];
            $tiempo_en_la_mesa=$_POST['tiempo_en_la_mesa'];
            $comensales=$_POST['comensales'];
            $insert= "INSERT INTO mesa (estado,numero_de_la_mesa,tiempo_en_la_mesa,comensales) VALUES ('" .$estado."','" .$numero_de_la_mesa. "','" .$tiempo_en_la_mesa. "','" .$comensales."')";
            $insertar=mysqli_query($con,$insert);
            if(!$insert){
                echo 'sea bienvenido';
            }
        }
    ?>
            <form method="post">
                <div class="tabla">
                    <h1>Crear mesa</h1>
                    <form action="caja.php" method="POST">
                            <label for="estado"></label>
                            <select name="estado" id="">
                                <option value="ocupada">ocupada</option>
                                <option value="disponible">disponible</option>
                            </select>
                            <br>
                            <br>
                            <label for="mesa deseada"></label>            
                            <input type="text" class="numero_de_la_mesa" name="numero_de_la_mesa" placeholder="numero de la mesa que desea">
                            <br>
                            <br>
                            <label for="tiempo deseado en la mesa"></label>
                            <input type="text" class="tiempo_en_la_mesa" name="tiempo_en_la_mesa" placeholder="tiempo que desea estar en la mesa">
                            <br>
                            <br>
                            <label for="numero de comensales para la mesa"></label>
                            <input type="text" class="comensales" name="comensales" placeholder="numero de comensales">
                            <br>
                            <br>
                            <button type="submit" class="guardar" name="Mesa">Listo</button>
                    </form>
                </div>
            </form>
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <table class="table" >
                    <thead class="table-success table-striped">
                        <tr>
                            <th>estado</th>
                            <th>numero_de_la mesa</th>
                            <th>tiempo_en_la_mesa</th>
                            <th>comensales</th>
                            <th></th>
                            <th></th>                                                                                
                        </tr>
                    </thead>
            </div>
        </div>
    </div>
                    <tbody>
                            <?php
                                $sql="SELECT * FROM mesa";
                                $result=mysqli_query($con,$sql);
                                while($mostrar=mysqli_fetch_array($result)){
                            ?>
                                <tr>
                                    <th><?php  echo $mostrar['estado']?></th>
                                    <th><?php  echo $mostrar['numero_de_la_mesa']?></th>
                                    <th><?php  echo $mostrar['tiempo_en_la_mesa']?></th>
                                    <th><?php  echo $mostrar['comensales']?></th>
                                    <th><a class="Edit" href="editar.php?id=<?php echo $mostrar['id'] ?>">Editar</a></th>
                                    <th><a class="remove" href="eliminar.php?id=<?php echo $mostrar['id'] ?>">Eliminar</a></th>
                                </tr>                                      
                            <?php 
                                }
                            ?>
                    </tbody>
                </table>
            </div>
        </div>
    <tbody> 
        <table>
            <tr>
                <?php
                $disponible = '';
                $ocupada = '';
                    $sql="SELECT * FROM mesa";
                    $result=mysqli_query($con,$sql);
                    while($mostrar=mysqli_fetch_array($result)){
                        switch($mostrar['estado']){
                            case 'disponible':
                                echo $disponible = '<div class="caja">
                                <div class="M1"><h6>' .$mostrar['numero_de_la_mesa']. '</h6></div>
                                <div><button class="boton_verde-1"></button></div>
                                <div class="icono_boton_azul-1"><img src="img/mesa.webp"></div>
                                <div class="personas"><h6>' .$mostrar['comensales']. '</h6></div>
                                </div>';
                            break;
                            default:echo $ocupada = '<div class="caja">
                                <div class="M1"><h6>' .$mostrar['numero_de_la_mesa']. '</h6></div>
                                <div><button class="boton_rojo-2"></button></div>
                                <div class="icono_boton_azul-1"><img src="img/mesa.webp"></div>
                                <div class="personas"><h6>' .$mostrar['comensales']. '</h6></div>
                                </div>';
                            break;
                        }
                    }
                ?>
            </tr>
        </table>
    </tbody>
</body>
</html>