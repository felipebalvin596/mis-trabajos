<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@300&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="css/estilos.css">
    <title>GALERIA</title>
</head>
<body>
    <header>
        <div class="contenedor">
            <h1 class="titulo">Mi Galeria en PHP y MYSQL</h1>

        </div>
    </header>

    <section class="fotos">
        <div class="contenedor">
            <div class="thumb">
                <a href="foto.php">
                    <img src="imagenes\waw.jpg" alt="">
                </a>
            </div>
            <div class="thumb">
                <a href="#">
                    <img src="imagenes\wew.jpg" alt="">
                </a>
            </div>
            <div class="thumb">
                <a href="#">
                    <img src="imagenes\wiw.jpg" alt="">
                </a>
            </div>
            <div class="thumb">
                <a href="#">
                    <img src="imagenes\wow.jpg" alt="">
                </a>
            </div>
            <div class="thumb">
                <a href="#">
                    <img src="imagenes\wuw.jpg" alt="">
                </a>
            </div>
            <div class="thumb">
                <a href="#">
                    <img src="imagenes\w1.jpg" alt="">
                </a>
            </div>
            <div class="thumb">
                <a href="#">
                    <img src="imagenes\w2.jpg" alt="">
                </a>
            </div>
            <div class="thumb">
                <a href="#">
                    <img src="imagenes\w3.jpg" alt="">
                </a>
            </div>
            <div class="thumb">
                <a href="#">
                    <img src="imagenes\w4.jpg" alt="">
                </a>
            </div>
            <div class="thumb">
                <a href="#">
                    <img src="imagenes\w5.jpg" alt="">
                </a>
            </div>
            <div class="thumb">
                <a href="#">
                    <img src="imagenes\1.jpg" alt="">
                </a>
            </div>
            <div class="thumb">
                <a href="#">
                    <img src="imagenes\12.jpg" alt="">
                </a>
            </div>
            <div class="paginacion">
                 <a href="#" class="izquierda"><i class="fa-solid fa-arrow-left-long"></i> Pagina anterior</a>
                 <a href="#" class="derecha">Siguiente pagina <i class="fa-solid fa-arrow-right"></i></a>
            </div>
        </div>
    </section>

    <footer>
        <p class="copyright">Galeria creada por Julian Felipe Balvin Bueno - cendi</p>
    </footer>
    <script src="https://kit.fontawesome.com/e579cc1cb2.js" crossorigin="anonymous"></script> 
</body>
</html>
