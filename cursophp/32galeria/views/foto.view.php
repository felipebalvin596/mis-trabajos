<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@300&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="css/estilos.css">
    <title>GALERIA</title>
</head>
<body>
    <header>
        <div class="contenedor">
            <h1 class="titulo">Foto: 1.jpg</h1>

        </div>
    </header>
    <div class="contenedor">
        <div class="foto">
            <img src="imagenes/1.jpg" alt="">
            <p class="texto">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ducimus, culpa.</p>
            <a href="index.php" class="regresar"><i class="fa-solid fa-arrow-left-long"></i> Regresar</a>
        </div>

    </div>

    <footer>
        <p class="copyright">Galeria creada por Julian Felipe Balvin Bueno - cendi</p>
    </footer>
    <script src="https://kit.fontawesome.com/e579cc1cb2.js" crossorigin="anonymous"></script> 
</body>
</html>
