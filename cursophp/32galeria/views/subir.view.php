<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@300&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="css/estilos.css">
    <title>GALERIA</title>
</head>
<body>
    <header>
        <div class="contenedor">
            <h1 class="titulo">Subir foto</h1>

        </div>
    </header>
    <div class="contenedor">
      <form class="formulario" method="POST" enctype="multipart/form-data" action="<?PHP echo htmlspecialchars($_SERVER['PHP_SELF']); ?>">
        
        <label for="foto">seleciona tu foto</label>
        <input type="file" name="foto" id="foto">

        <label for="titulo">titulo de foto</label>
        <input type="text" name="titulo" id="titulo">

        <label for="texto">descripcion:</label>
        <textarea name="texto" id="texto" placeholder="ingresa una descripcion"></textarea>

        <?php if(isset($error)): ?>
        <p class="error"> <?php echo $error; ?></p>
        <?php endif ?>

        <input type="submit" class="submit" value="subir foto">
    
    
      </form> 
    
    </div>
    <footer>
        <p class="copyright">Galeria creada por Julian Felipe Balvin Bueno - cendi</p>
    </footer>
    <script src="https://kit.fontawesome.com/e579cc1cb2.js" crossorigin="anonymous"></script> 
</body>
</html>
