<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Inconsolata:wght@300&family=Noto+Serif:ital@1&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="css/estilo.css">
        <title>blog</title>
    </head>
    <body>
    <header>
        <div class="contenedor">
            <div class="logo izquierda">
                <p><a href="index.php"> mi blog </a></p>
            </div>
            <div class="derecha">
                <form name="busqueda" class="buscar" action="/buscar.php" metho="get">
                    <input type="text" name="busqueda" placeholder="Buscar">
                    <button type="submit" class="icono fa fa-search"></button>
                </form>
                <nav class="menu">
                    <ul>
                        <li><a href="#"><i class="fa-brands fa-twitter"></i></a></li>

                        <li><a href="#"><i class="fa-brands fa-facebook-f"></i></a></li>

                        <li><a href="#">contactanos <i class="icono fa fa-envelope"></i></a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </header>


    </body>
</html>