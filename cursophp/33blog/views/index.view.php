<?php
require 'header.php';
?>

    <div class="contenedor">
        <div class="post">
            <article>
                <h2 class="titulo"><a href="#"> Titulo del blog </a></h2>
                <p class="fecha">90 de diciembre del 2903</p>
                <div class="thumb">
                    <a href="#"><img src="img/noseasapo.jpg" alt=""></a>
                </div>
                <p class="estracto"> Lorem ipsum dolor sit amet consectetur adipisicing elit. Cupiditate voluptatibus ab incidunt ipsam rem et, odit pariatur, ex eius, mollitia maxime vitae reiciendis. Provident aspernatur aliquam voluptates commodi quaerat qui!</p>
                <a href="#" class="continuar"> Continuar leyendo</a>
            </article>
        </div>
        <div class="post">
            <article>
                <h2 class="titulo"> Titulo del blog</h2>
                <p class="fecha">822 de diciembre del 2403</p>
                <div class="thumb">
                    <a href="#"><img src="img/noseasapo.jpg" alt=""></a>
                </div>
                <p class="estracto"> Lorem ipsum dolor sit amet consectetur adipisicing elit. Cupiditate voluptatibus ab incidunt ipsam rem et, odit pariatur, ex eius, mollitia maxime vitae reiciendis. Provident aspernatur aliquam voluptates commodi quaerat qui!</p>
                <a href="#" class="continuar"> Continuar leyendo</a>
            </article>
        </div>
        <?php require 'paginacion.php'; ?>

    </div>

    <?php require 'footer.php';?>


 
</html>