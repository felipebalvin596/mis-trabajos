<?php
date_default_timezone_set('america/Bogota');
echo date('a'). '<br/>';//Formato am-pm
echo date('e'); '<br/>';//Formato am-pm
echo '<br/>';

echo date('B'). '<br/>';//SWATCH INTERNET TIME
echo '<br/>';

echo date('g'). '<br />';//Formato de 12 horas
echo date('G'); '<br />';//Formato de 24 horas
echo '<br/>';

echo date('h'). '<br />';//Formato de 12 horas con ceros
echo date('H'); '<br />';//Formato de 24 horas con ceros
echo '<br/>';

?>