<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@200;300;400;500;600;700&family=Raleway:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Roboto+Condensed:wght@300&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/a8dc9e0e5d.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="css/estilos.css">
    <title>Iniciar sesion</title>
</head>
<body>
    <div class="contenedor">
        <h1 class="titulo">Iniciar sesion</h1>
        <hr class="border">
        <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="POST" class="formulario" name="login">
          <div class="form-group">
            <i class="icono izquierda fa-solid fa-user"></i><input type="text" name="usuario" class="usuario" placeholder="Usuario">
          </div>

         

          <div class="form-group">
          <i class="icono izquierda fa-solid fa-lock"></i><input type="password" name="password" class="password_btn" placeholder="Contraseña">
          <i class= "submit-btn fa fa-arrow-right" onclick="login.submit()"></i>
          </div>


          <?php if(!empty($errores)): ?>
          <div class="error">
            <ul>
              <?php echo $errores; ?>
            </ul>
          </div>
          <?php endif; ?>



        </form>
        <p class="texto-registrate">
          ¿aun no tienes cuenta?
          <a href="registrate.php">Registrate</a>

        </p>
         </div>

    </div>
</body>
</html>