<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/estilos.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@200;300;400;500;600;700&family=Raleway:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Roboto+Condensed:wght@300&display=swap" rel="stylesheet">
    <title></title>
</head>
<body>
    <div class="contenedor">
        <h1 class="titulo">CONTENIDO DEL SITIO</h1>
        <a href="cerrar.php">CERRAR SECCION</a>
        <hr class=border>
        <div class="contenido">
            
            <article>
                <p>PLorem ipsum dolor sit amet consectetur adipisicing elit.
                    Ratione accusamusfugit nobis hic asperiores
                    necessitatibus atque earum dolorem minus corrupti
                    aliquam libero architecto tempora, tempore
                    quidem suscipit, odio ex enim!
                </p>

                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.
                        Ab, consequuntur omnis! Totam deleniti ipsum minus quae
                        ab eligendi harum dolorem fuga illo nesciunt iure
                        quaerat vero ea amet, repellat consectetur?
                    </p>

                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Officia, ex. Soluta repudiandae saepe fuga animi earum odio praesentium at quaerat unde? Labore quasi non totam beatae dolor, placeat est distinctio.
                            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Labore dignissimos, quas asperiores rerum aspernatur quis dicta, voluptas facilis, ratione quidem facere natus nihil veniam pariatur blanditiis recusandae placeat suscipit cupiditate.
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Numquam ex quibusdam laudantium eum, tempora quisquam tempore quasi accusamus impedit non corrupti voluptatum eveniet maxime iste cupiditate. Culpa impedit eaque qui.
                        </p>
            </article>
        </div>

    </div>
</body>
</html>