-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 31-05-2022 a las 18:00:40
-- Versión del servidor: 10.4.22-MariaDB
-- Versión de PHP: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `porcentaje_2`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `porcentaje_1`
--

CREATE TABLE `porcentaje_1` (
  `id` int(11) NOT NULL,
  `Cc` int(11) NOT NULL,
  `precioArt_1` int(100) NOT NULL,
  `precioArt_2` int(100) NOT NULL,
  `precioArt_3` int(100) NOT NULL,
  `descuento` int(20) NOT NULL,
  `total` int(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `porcentaje_1`
--

INSERT INTO `porcentaje_1` (`id`, `Cc`, `precioArt_1`, `precioArt_2`, `precioArt_3`, `descuento`, `total`) VALUES
(1, 43734877, 34, 98, 45, 4, 177),
(2, 1232131, 2147483647, 2147483647, 123123, 4, 0),
(3, 1001234439, 40, 653, 38, 5, 0),
(4, 1001234439, 40, 653, 38, 5, 0),
(5, 1001234439, 40, 653, 38, 5, 0),
(6, 1001234439, 40, 653, 38, 5, 0),
(7, 1001234456, 20, 55, 38, 10, 0),
(8, 1001234456, 40, 55, 12, 10, 300),
(9, 43677899, 20, 5, 2, 3, 0),
(10, 1001234456, 20, 10, 5, 10, 100),
(11, 1001234456, 20, 10, 5, 10, 100);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `porcentaje_1`
--
ALTER TABLE `porcentaje_1`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `porcentaje_1`
--
ALTER TABLE `porcentaje_1`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
