<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php

    try{   /*  try= intenta. y este le esta diciendo al codigo. (intenta conetar con la base de datos y en caso de que no lo consigas 
        me cojes el objeto del error y me haces lo que te diga (catch))  */
    $base=new PDO('mysql: host=localhost; db_name=prueba', 'root', '');
    echo "conexion exitosa";  // si se genera algun error antes de esta linea esta linea noo se va a ejecutar y saltaria directamente al catch
    } catch(Exception $e){
        die('Error: ' . $e->GetMessage());  // die=matame. 
    }finally{    // este imprime lo que tenga dentro sin importar si tengo error o no
        $base=null;
    }
    ?>
</body>
</html>