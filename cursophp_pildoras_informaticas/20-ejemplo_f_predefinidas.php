<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php

    /*  $palabra="JULIAN";  
    echo (strtolower($palabra));   */  // cambio de minuscula a mayuscula

    /*  function suma ($num1, $num2){

    $resultado=$num1+$num2; // este me devuelve el resultado que suman las dos variables

    return $resultado;

    }

    echo(suma(4,4));  */ 

    function frase_mayuscula($frase,$conversion=true){
    $frase=strtolower($frase);
    if($conversion==true){
        $resultado=ucwords($frase);
    }else{
        $resultado=strtoupper($frase);
    }
    return $resultado;
    }

    /* echo (frase_mayuscula("esto es de prueba")); */

    echo (frase_mayuscula("esto es de prueba",false));

    ?>
</body>
</html>