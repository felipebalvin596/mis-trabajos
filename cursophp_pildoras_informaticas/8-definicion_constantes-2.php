<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Constante</title>
</head>
<body>
<?php

define("AUTOR", "Juan");

echo "La linea de esta instruccion es: " . __LINE__ . "<br>";

echo "Estamos trabajando con este fichero: " . __FILE__;

?>
</body>
</html>