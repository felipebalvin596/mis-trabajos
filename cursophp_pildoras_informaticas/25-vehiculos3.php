<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<?php

class Coche{

    // estas son sus propiedades

    protected $ruedas; //  este hace que la variable solo sea accesible desde la propia clase y no nos permite ordenarle nada desde afuera de esta linea

    var $color;

    protected $motor;

    function coche(){ //  metodo constructor

        // $this tiene refeerencia expecifica a la clase coche

        $this->ruedas=4;
        $this->color="";
        $this->motor=1800;
    }

    function get_motor(){
        return $this->motor;
    }

    function get_ruedas(){
        return $this->ruedas;
    }

    function arrancar(){  
        echo "estoy arrancando <br>";
    }
    function girar(){
        echo "estoy girando <br>";
    }
    function ruedas(){
        echo "tengo 4 ruedas <br>";
    }
    function estabecer_color($color_coche, $nombre_coche){
        $this->color=$color_coche;
        echo "El color del " . $nombre_coche . " es: " . $this->color . "<br>";
    }

}


//  -------------------------------------------------------------------------------------------------------------

class camion extends coche{

    function camion(){
        $this->ruedas=8;
        $this->color="gris";
        $this->motor=2800;
    }

    function estabecer_color($color_camion, $nombre_camion){
        $this->color=$color_camion;
        echo "El color del " . $nombre_camion . " es: " . $this->color . "<br>";
    } 

    function arrancar(){
        parent::arrancar();
        echo "Camion arrancando";
        
    }
}