<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php

    class Coche{

        // estas son sus propiedades

        var $ruedas;

        var $color;
    
        var $motor;

        function coche(){ //  metodo constructor

            // $this tiene refeerencia expecifica a la clase coche

            $this->ruedas=4;
            $this->color="";
            $this->motor=1800;
        }

        function arrancar(){  
            echo "estoy arrancando <br>";
        }
        function girar(){
            echo "estoy girando <br>";
        }
        function ruedas(){
            echo "tengo 4 ruedas <br>";
        }
        function estabecer_color($color_coche, $nombre_coche){
            $this->color=$color_coche;
            echo "El color del " . $nombre_coche . " es: " . $this->color . "<br>";
        }

    }

    //  estas es una instancia perteneciente a la clase coche

    $renult= new coche();  //  estado inicial al objeto o instancia

    $mazda= new coche();

    $seat= new coche();

     // estoy llamando al objeto para que haga la tarea indicada

    $renult->estabecer_color("rojo","renul");
    $seat->estabecer_color("azul","seat"); 
    
    // $mazda->girar(); 

    // echo $mazda->ruedas();

    // echo $mazda->arrancar();

    ?>
</body>
</html>