<?php
    
    $pais = $_GET['buscar'];
    require ('42-datos_conexion.php');
    $conexion=mysqli_connect($db_host, $db_usuario, $db_contraseña);

    if(mysqli_connect_errno()){
        echo"fallo al conectar con la base de datos";
        exit();
    }

    mysqli_select_db($conexion,$db_nombre) or die ("no se encuentra la base de datos");
    mysqli_set_charset($conexion, "utf8");

    $sql="SELECT codigoarticulo, seccion, precio, paisorigen FROM producto3 WHERE paisorigen= ? ";

    $resultado=mysqli_prepare($conexion, $sql);

    $ok=mysqli_stmt_bind_param($resultado, "s", $pais);

    $ok=mysqli_stmt_execute($resultado);

    if($ok==false){
        echo "error al ejecutar la consulta";
    }else{
        $ok=mysqli_stmt_bind_result($resultado, $codigo, $seccion, $precio, $pais);

        echo "Articulos encontrados: <br><br>";

        while(mysqli_stmt_fetch($resultado)){
            echo $codigo . " " . $seccion . " " . $precio . " " . $pais . "<br>";
        }

        mysqli_stmt_close($resultado);
    }

    ?>