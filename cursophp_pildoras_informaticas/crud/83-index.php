<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="hoja.css">
    <title>CRUD</title>
</head>
<body>
    <?php

    include "84-conexion.php";
    
    /*  $conexion=$base->query("SELECT * FROM datos_usuarios");
    $registro=$conexion->fetchAll(PDO::FETCH_OBJ);   */

    // ----------------------------- PAGINACION --------------------------- //

    $tamaño_paginas=3;

        if(isset($GET['pagina'])){
            if ($_GET['pagina']==1){
                header("location:87-index.php");
            }else{
                $pagina=$_GET['pagina'];
            }
        }else{
        $pagina=1;
        }
        
        $empezar_desde=($pagina-1)*$tamaño_paginas;
        //  $sql_total="SELECT nombrearticuo, seccion, precio, paisorigen FROM producto3 WHERE SECCION='DEPORTES' LIMIT 0,3"; // este me muestra las vences que el registro me devuelve la consulta
        $sql_total="SELECT nombrearticuo, seccion, precio, paisorigen FROM producto3 WHERE SECCION='DEPORTES' ";  // este me muestra las vences que el registro me devuelve la consulta
        $resultado=$base->prepare($sql_total);
        $resultado->execute(array());
        $num_filas=$resultado->rowCount();
        $total_paginas=ceil($num_filas/$tamaño_paginas);
        //--------------------------------------------------------------//

    $registros=$base->query("SELECT * FROM datos_usuarios")->fetchAll(PDO::FETCH_OBJ);

    if(iseet($_POST['bott'])){
        $nombre=$_POST['nom'];
        $apellido=$_POST['ape'];
        $direccion=$_POST['dir'];
        $sql="INSERT INTO datos_usuarios (nombre, apellido, direccion) VALUE (:nom, :ape, :dir,)";
        
        $resultado=$base->prepare($sql);
        $resultado->execute(array(":nom"=>$nombre, ":ape"=>$apellido, ":dir"=>$direccion));
        header("location:83-index.php");
    }

    ?>
    <h1>crud<span class="subtitulo">create read update delete</span></h1>
    <form action="<?php echo $_SERVER['PHP_SELF'];?>" method="POST">
        <table width="50%" border="0" align="center">

            <tr>
                <td class="primera_fila">id</td>
                <td class="primera_fila">nombre</td>
                <td class="primera_fila">apellido</td>
                <td class="fin">#</td>
                <td class="fin">#</td>
                <td class="fin">#</td>
            </tr>

            <?php
                foreach($registro as $persona):?>

            <tr>
                <td><?php echo $persona->id ?></td>
                <td><?php echo $persona->nombre ?></td>
                <td><?php echo $persona->apellido ?></td>
                <td><?php echo $persona->direccion ?></td>
                
                <td class="bot"><a href="85-eliminar.php?id=<?php echo $persona->id?>"><input type="button" name="del" id="del" value="Borrar"></a></td>
                <td class="bot"><a href="86-editar.php?id=<?php echo $persona->id?> & nom=<?php echo $persona->nombre ?> & ape=<?php echo $persona->apellido ?> 
                & dir=<?php echo $persona->direccion ?>"><input type="button" name="up" id="up" value="Actualizar"></a></td>
            </tr>

            <?php
                endforeach;
            ?>

            <tr>
                <td></td>
                <td><input type="text" name="Nom" size="10" class="centrado"></td>
                <td><input type="text" name="Ape" size="10" class="centrado"></td>
                <td><input type="text" name="Dir" size="10" class="centrado"></td>
                <td class="bot"><input type="submit" name="bott" id="bott" value="insertar"></td>
            </tr>
        </table>
    </form>
    <p>#</p>
</body>
</html>