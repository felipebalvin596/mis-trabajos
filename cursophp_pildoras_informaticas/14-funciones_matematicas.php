<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
    /*  $num1=rand(1, 30);   // este me da numeros aleatorios y si le pongo numeros me los da dentro de los numeros que yo le ponga.
    echo "El numero es: " . $num1;   */


    /*  $num1=pow(7,5);    // este me guia la potencia de numeros dependiendo como yo se los ponga.
    echo "El numero es: " . $num1;  */


    /*  $num1=3.65; 
    echo "El numero es: " . round($num1,2);   //  est2 me redondea la numeracion que le ponga y si le pongo el numero 2 despues de la bariable eso significa que quiero que me de dos decimales.  */


    $num1="4";   // este es una bariable de tipo texto entonces me imprime el numero que tenga.

    $num1+=3;   // este es un operador de incremento. y php ya asume que $num1 es de tipo entero.

    $num1+5.85;  // este asume que desde esta linea en adelante es de tipo float, es decir que es decimal.


    echo "El numero es: " . $num1;


    ?>
</body>
</html>