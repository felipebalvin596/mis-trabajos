<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
    $conexion=new mysqli("localhost", "root", "", "prueba");
    if($conexion->connect_errno){
        echo "fallo la conexion" . $conexion->connect_errno;
    }

    //    mysqli_set_charset($conexion, "utf8");
        $conexion->set_charset("utf8");
        $sql="SELECT * FROM producto3";
    //    $resultados=mysqli_query($conexion, $sql);
        $resultados=$conexion->query($sql);
        if($conexion->errno){
            die($conexion->error);
        }

    //    while($fila=mysqli_fetch_array($resultados, MYSQLI_ASSOC)){

            while($fila=$resultados->fetch_assoc()){   /*   este es un array asociativo me permite asociar los campos que me devuelve lo que esta dentro de la bariable fila, 
                esto si lo ponemos  con arrar y con lo campos en numerados me va a imprimir lo mismo. las diferencicias entre estos es que uno es un array asociativo y el otro un array */
                
                echo "<table><tr><td>";

                echo $fila['codigoarticulo'] . "</td><td> ";
                echo $fila['nomberarticulo'] . "</td><td> ";
                echo $fila['seccion'] . "</td><td> ";
                echo $fila['importado'] . "</td><td> ";
                echo $fila['precio'] . "</td><td> ";
                echo $fila['paisorigen'] . "</td><td></tr></table> ";
                
                echo "<br>";
                echo "<br>";
            }

    mysqli_close($conexion);  //  forma procedimental para cerrar la conexion

    $conexion->close();  // forma de programacion orientada objetos
    
    ?>
</body>
</html>