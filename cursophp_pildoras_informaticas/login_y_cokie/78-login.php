<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <style>
        h1,h2{
            text-align: center;
        }
        table{
            width: 25%;
            background-color: #ffc;
            border: 2px dotted #f00;
            margin: auto;
        }
        .izq{
            text-align: right;
        }
        .der{
            text-align: left;
        }
        td{
            text-align: center;
            padding: 10px;
        }
    </style>
</head>
<body>

    <?php

     $autenticado=false;   /* esta es una bariable buleana, que le indica al codigo que si el usuario a tenido exito al loguearse que guarde sus datos y pasa a ser true*/

        if(isset($_POST['enviar'])){
        try{
            $base=new PDO("mysql:host=localhost, bdname=prueba", "root", "");
            $base->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql="SELECT * FROM usuarios_pass WHERE USUARIOS= :login AND PASSWORD= :password";
            $resultado=$base->prepare($sql);
            $login=htmlentities(addslashes($_POST['login']));   // "htmlentities" puede combertir caulquier simbolo en html
            $password=htmlentities(addslashes($_POST['password'])); 
            $resultado->bindValue(":login", $login);
            $resultado->bindValue(":password", $password);
            $resultado->execute();
            $numero_registro=$resultado->rowCount();   // "rowCount", lo que hace es decirme el numero de registros que devuelve una consulta

            if($numero_registro!=0){
                 $autenticado=true;
                 
                 if(isset($_POST["recordar"])){
                    setcookie("nombre_usuario", $_POST["login"], time()+86400);
                 }
            }else{
            echo "Error. usuario o contraseña incorrectos";
            }

        }catch(Exception $e){

            die ("Error: " . $e->getMessage());
        }
        }
    ?>

    <?php
        if($autenticado==false){
            if(!isset($_COOKIE["nombre_usuario"])){
                include("67-formulario.html");
            }
        }

        if(isset($_COOKIE["nombre_usuario"])){  // esta linea me le indica al codigo que en el caso de que se haya creado la cookie salude al usuario con lo que tenga almacebado la cookie
            echo "!Hola " . $_COOKIE["nombre_usuario"] . "!";

        }else if($autenticado==true){
            echo "!Hola " . $_POST["login"] . "!";
        }        
    ?>
        
        <h2>CONTENIDO DE LA WEB</h2>
        <table width="800" border="0">
            <tr>
                <td><img src="imagenes/w1.jpg" width="300" height="116"></td>
                <td><img src="imagenes/wiw.jpg" width="300" height="171"></td>
            </tr>
            <tr>
                <td><img src="imagenes/wow.jpg" width="300" height="166"></td>
                <td><img src="imagenes/wuw.jpg" width="300" height="197"></td>
            </tr>
        </table>

        <?php
            if($autenticado==true || isset($_COOKIE["nombre_usuario"])){
                include("80-zona_registrados.html");
            }
        ?>
</body>
</html>