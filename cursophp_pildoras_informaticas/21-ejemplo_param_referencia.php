<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
    /*  function incrementa(&$valor1){
        $valor1++;
        return $valor1; // si en esta linea le pongo incremento me imprime un numero meyoy que otro
    }
    $numero=5;
    // echo incrementa(9);
    echo incrementa($numero) . '<br>';  //  este me incermenta el valor1 9 veces mas
    
    echo $numero;  // $numero=5 va a $valor1, valor se incrmenta a 6 y $numero tambien se incrementa porque esta vinculado  */


    function cambia_mayus($param){ // & si pongo este signo, le estoy ordenando a mi codigo que me lo reciba por referencia y me va a ejecutar todo en el mismo orden

        $param=strtolower($param);

        $param=ucwords($param);

        return $param;
    }
    $cadena="hOlA mUnDo";

    echo cambia_mayus($cadena) . '<br>';   // esta linea de codigo me imprime lo que tengo en la variable $cadena acomodandolo con las primeras letras de cada palabra en meyuscula

    echo $cadena; // esta linea de codigo me imprime exactamente lo que tengo en la variable $cadena
    ?>
</body>
</html>