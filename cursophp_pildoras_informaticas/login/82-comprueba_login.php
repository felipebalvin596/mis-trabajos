<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php

    try{
        $login=htmlentities(addslashes($_POST['login']));   // "htmlentities" puede combertir caulquier simbolo en html
        $password=htmlentities(addslashes($_POST['password'])); 
        $contador=0;
        $base=new PDO("msql:host=localhost; dbname=prueba" , "root", "");
        $base->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $sql="SELECT * FROM usuarios_pass WHERE usuarios= :login";
        $resultado=$base->prepare($sql);
        $resultado=execute(array(":login"=>$login));
        while($registro=$resultado->fetch(PDO::FETCH_ASOCC)){
            if (password_verify($password, $registro['password'])){
                $contador++;
            }
        }
        if($contador>0){
            echo"usuario registrado";

        }else{
            echo"usuario no registrado";
        }
    }
    ?>
</body>
</html>