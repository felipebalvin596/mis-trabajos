<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php

    //  si le pongo numeros a los indices de los dias me toca especificar el numero que tiene el indice del dia que quiero imrprimir si no los dias cuentan desde 0 y van en orden

    /* $semana[]="lunes";

    $semana[]="martes";

    $semana[]="miercoles";

    $semana[]="jueves";

    $semana[]="viernes";

    echo $semana[3];   */ // (1)


    /* $semana=array("lunes","martes","miercoles","jueves","viernes"); // esto me imprime lo mismo solo que es una forma diferente para hacerlo

    echo $semana[4];   */ // (2)


    $datos=array("nombre"=>"pedro","apellido"=>"velez","edad"=>23);

    /*  echo $datos["nombre"];
    echo $datos["apellido"];
    echo $datos["edad"]; */ //3

    $datos="martin";
    echo $datos;

    ?>
</body>
</html>