<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php

    try{

        $base=new PDO("mysql:host=localhost, bdname=prueba", "root", "");
        $base->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql="SELECT * FROM usuarios_pass WHERE USUARIOS= :login AND PASSWORD= :password";
        $resultado=$base->prepare($sql);
        $login=htmlentities(addslashes($_POST['login']));   // "htmlentities" puede combertir caulquier simbolo en html
        $password=htmlentities(addslashes($_POST['password'])); 
        $resultado->bindValue(":login", $login);
        $resultado->bindValue(":password", $password);
        $resultado->execute();
        $numero_registro=$resultado->rowCount();   // "rowCount", lo que hace es decirme el numero de registros que devuelve una consulta

        if($numero_registro!=0){
            //echo "<h2>Adelante!!</h2>";
            session_start();

            $_SESSION['usuario']=$_POST['login'];
                        
            header("location: 61-usuario_registrado1.php");
        }else{
            header("location: 59-sistema_login.php");
        }

    }catch(Exception $e){

        die ("Error: " . $e->getMessage());

    }
    ?>
</body>
</html>