<?php
require ("config.php");

class conexion{
    protected $conexion_db;

    public function conexion(){

        try{
            $this->conexion_db=new PDO('mysqli:host=localhost; dbname=pruba', 'root', '');
            $this->conexion_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->conexion_db->exec("SET CHARACTER SET utf8");

            return $this->conexion_db;

        }catch(exception $e){
            echo "la linea de error es: " . $e->getLine();
        }

    }
}
?>